package dare.components.registry;

import io.kubernetes.client.ApiClient;
import io.kubernetes.client.ApiException;
import io.kubernetes.client.Configuration;
import io.kubernetes.client.apis.CoreV1Api;
import io.kubernetes.client.models.V1Pod;
import io.kubernetes.client.models.V1PodList;
import io.kubernetes.client.proto.V1.List;
import io.kubernetes.client.util.Config;
import io.sundr.shaded.com.github.javaparser.ast.expr.ThisExpr;

import java.io.IOException;


public class podParserMain {
	/* Hostname must include port, e.g http://localhost:8080 */
	public static java.util.List<V1Pod> getPodsApi(String hostname) throws IOException, ApiException
	{
        ApiClient client = Config.defaultClient().setBasePath(hostname);
        Configuration.setDefaultApiClient(client);

        CoreV1Api api = new CoreV1Api();
        V1PodList list = api.listPodForAllNamespaces(null, null, null, null, null, null, null, null, null);
        return list.getItems();
	}
	
	/* Print pods to check if parsing was completed */
    public static void main(String[] args) throws IOException, ApiException{
    	java.util.List<V1Pod> list = getPodsApi(args[0]);
        for (V1Pod item : list) {
            System.out.println(item.getMetadata().getName());
        }
    }
}
